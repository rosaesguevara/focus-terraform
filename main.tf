terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

resource "digitalocean_kubernetes_cluster" "focus-cluster" {
    name   = "focus-cluster"
    region = "nyc1"
    version = "1.21.5-do.0"
  
    node_pool {
      name       = "worker-pool"
      size       = "s-2vcpu-2gb"
      node_count = 2
    }
  }

  resource "digitalocean_container_registry_docker_credentials" "focus-registry" {
  registry_name = "focus-registry"
}